@extends('layouts.app')

@section('content')
<h1>Grupo (lista en sesión)</h1>
<ul>

<table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Role</th>
            <th>Cantidad</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role->name }}</td>
            <td>{{ $user->cantidad }}</td>
            <td>

              <form method="post" action="/users/{{ $user->id }}">
                @can('update', $user)
                <a class="btn btn-primary"  role="button"
                href="/users/{{ $user->id }}/edit">
                Editar
              </a>
              @endcan
              <a class="btn btn-primary"  role="button"
              href="/users/{{ $user->id }}">
              Ver
            </a>
            {{ csrf_field() }}
            @can('delete', $user)
            <input type="hidden" name="_method" value="DELETE">
            <input type="submit" value="Borrar" class="btn btn-primary">
            @endcan
          </form>
        </td>
      </tr>
      @empty
      <tr><td colspan="4">No hay usuarios</td></tr>
      @endforelse
    </tbody>
  </table>
<a href="/groups/flush" class="btn btn-danger">Vaciar lista</a>
{{--   {{ $users->render() }} --}}
</div>
</div>
</div>
@endsection
