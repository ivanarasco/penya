@extends('layouts.app')

@section('content')
<h1>Lista de roles</h1>

<ul>

<table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($roles as $role)
          <tr>
            <td>{{ $role->id }}</td>
            <td>{{ $role->name }}</td>
            <td>

              <form method="post" action="/roles/{{ $role->id }}">
                @can('edit', $role)
                <a class="btn btn-primary"  role="button"
                href="/roles/{{ $role->id }}/edit">
                Editar
              </a>
              @endcan
              @can('view', $role)
              <a class="btn btn-primary"  role="button"
              href="/roles/{{ $role->id }}">
              Ver
            </a>
            @endcan
            {{ csrf_field() }}
            @can('delete', $role)
            <input type="hidden" name="_method" value="DELETE">
            <input type="submit" value="Borrar" class="btn btn-primary">
            @endcan
          </form>
        </td>
      </tr>
      @empty
      <tr><td colspan="4">No hay roles</td></tr>
      @endforelse
    </tbody>
  </table>
<a href="/roles/create" class="btn btn-primary">Nuevo</a>
  {{ $roles->render() }}
</div>
</div>
</div>
@endsection
