@extends('layouts.app')

@section('content')
<h1>Lista de Categorías</h1>
<ul>

  <table  class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($categorias as $cathegory)
      <tr>
        <td>{{ $cathegory->id }}</td>
        <td>{{ $cathegory->name }}</td>
        <td>

          <form method="post" action="/cathegories/{{ $cathegory->id }}">
            @can('edit', $cathegory)
            <a class="btn btn-primary"  role="button"
            href="/cathegories/{{ $cathegory->id }}/edit">
            Editar
          </a>
          @endcan
          <a class="btn btn-primary"  role="button"
          href="/cathegories/{{ $cathegory->id }}">
          Ver
        </a>
        {{ csrf_field() }}
        @can('delete', $cathegory)
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" value="Borrar" class="btn btn-primary">
        @endcan
      </form>
    </td>
  </tr>
  @empty
  <tr><td colspan="4"><strong>No hay Categorías</strong></td></tr>
  @endforelse
</tbody>
</table>
@can('create', App\Cathegory::class)
<a href="/cathegories/create" class="btn btn-primary">Nuevo</a>
@endcan
{{ $categorias->render() }}
</div>
</div>
</div>
@endsection
