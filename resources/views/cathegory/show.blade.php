@extends('layouts.app')

@section('title', 'Categoría')

@section('content')

    <h1>
        Este es el detalle de la Categoría: <?php echo $cathegory->name ?>
    </h1>

    <hr>

    <h2> Lista de Productos </h2>

    @forelse ($cathegory->products as $product)
    <ul>
       <li> {{ $product->name }} </li>
    </ul>
    @empty
    <strong> No hay ningún producto asociado a esta categoría. </strong>
    @endforelse
@endsection

