@extends('layouts.app')

@section('title', 'Productos')

@section('content')

    <h1>
        Este es el detalle del producto: <?php echo $product->name ?>
    </h1>

    <ul>
        <li>Nombre: {{ $product->name }}</li>
        <li>Precio: {{ $product->price }}</li>
        <li>Categoría: {{$product->cathegory->name}}
    </ul>

@endsection

