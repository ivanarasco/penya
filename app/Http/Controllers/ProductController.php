<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Cathegory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $products = Product::paginate(10);
        return view('product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $currentUser = \Auth::user();
        if ($currentUser->can('create', Product::class)) { // la diferencia entre el authorize y ésto es que el can te redirecciona a la página que quieras, en cambio, el authorize manda a un 403 siempre.
            $categorias = Cathegory::all();
            return view('product.create', ['categorias' => $categorias]);
        }else{
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validación:
        $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required',
            'cathegory_id' => 'required', // un select
        ];

        $request->validate($rules);

        $product = new Product();
        $product->fill($request->all());
        $product->save();

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.show', [
            'product' => $product,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currentUser = \Auth::user();
        if ($currentUser->can('edit', Product::class)) {
           $product = Product::findOrFail($id);
           $categorias = Cathegory::all();
           return view('product.edit', ['product' => $product], ['categorias' => $categorias]);
       }else{
        return back();
    }
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $rules = [
        'name' => 'required|max:255|min:3',
        'price' => 'required',
            'cathegory_id' => 'required', // un select
        ];

        $request->validate($rules);

        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();


        return redirect('/products/' . $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

           $product = Product::findOrFail($id); // (el que hemos intentado borrar)
           $this->authorize('delete', $product);
           $product->delete();
           return back();
       }
   }

