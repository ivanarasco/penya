<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session; // para que el namespace utilice la sesión.

class GroupController extends Controller // es un listado de usuarios que metemos en sesión (group)
{
    public function index(Request $request){
        //dd(Session::all());
        //$request->session()->forget('group'); // para borrar.
        $group = $request->session()->get('group');

        if (! $group){
            $group = [];
        }
        //dd($group);
        return view('group.index', ['users' => $group]);
    }

    public function addUser(Request $request, $id){
        $user = User::findOrFail($id);

        $group = $request->session()->get('group'); // es un array

        if ($group == null){
            $group = array();
        }

        $position = -1;
        foreach ($group as $item => $key){ //evitar duplicados.
            if ($item->id == $id) {
                $item->cantidad++;
                $position = $key;
                break;
            }
        }

        if ($position == -1) {
            $user->cantidad = 1;
            $request->session()->push('group', $user);
        }

        return redirect('/groups');
    }

    public function flush(Request $request){ // borrar lista de la sesión
        $request->session()->forget('group');
        return redirect('/users');
    }

}
